defineProperty("soundCap", globalPropertyi("parshukovedition/soundCap"))
defineProperty("isalerton", globalPropertyi("parshukovedition/isalerton"))
defineProperty("beacon_up", globalPropertyi("parshukovedition/beacon_up"))
defineProperty("beacon_down", globalPropertyi("parshukovedition/beacon_down"))
defineProperty("test_lamp_pilot", globalPropertyi("parshukovedition/test_lamp_pilot"))
defineProperty("test_lamp_pilot1_switch", globalPropertyi("parshukovedition/test_lamp_pilot1_switch"))
defineProperty("test_lamp_pilot2_switch", globalPropertyi("parshukovedition/test_lamp_pilot2_switch"))
defineProperty("testmsrp", globalPropertyi("parshukovedition/testmsrp"))
defineProperty("testmsrp_sound_switch", globalPropertyi("parshukovedition/testmsrp_sound_switch"))
defineProperty("testmsrp_cap", globalPropertyi("parshukovedition/testmsrp_cap"))
defineProperty("testmsrp_sound_switch_cap", globalPropertyi("parshukovedition/testmsrp_sound_switch_cap"))
defineProperty("msrp_switch", globalPropertyi("parshukovedition/msrp_switch"))
defineProperty("msrp_sound_switch", globalPropertyi("parshukovedition/msrp_sound_switch"))
defineProperty("msrp_switch_cap", globalPropertyi("parshukovedition/msrp_switch_cap"))
defineProperty("msrp_sound_switch_cap", globalPropertyi("parshukovedition/msrp_sound_switch_cap"))
defineProperty("msrplight", globalPropertyi("parshukovedition/msrplight"))
defineProperty("green_led", loadImage("leds.png", 20, 0, 20, 20))
defineProperty("bus_DC_27_volt", globalPropertyf("sim/custom/xap/An24_power/bus_DC_27_volt"))
defineProperty("flight_time", globalPropertyf("sim/time/total_running_time_sec"))
defineProperty("autopilot_state_PF", globalPropertyf("parshukovedition/autopilot_state_PF"))
defineProperty("autopilot_state_FO", globalPropertyf("parshukovedition/autopilot_state_FO"))
defineProperty("autopilot_state_PF_button", globalPropertyi("parshukovedition/autopilot_state_PF_button"))
defineProperty("autopilot_state_FO_button", globalPropertyi("parshukovedition/autopilot_state_FO_button"))
defineProperty("PF_ApbuttonState", globalPropertyf("parshukovedition/autopilot_state_PF_ApbuttonState"))
defineProperty("FO_ApbuttonState", globalPropertyf("parshukovedition/autopilot_state_FO_ApbuttonState"))
defineProperty("ap_on_lit", globalPropertyi("sim/custom/xap/An24_ap/ap_on_lit"))
defineProperty("nosmokingswitch", globalPropertyi("parshukovedition/nosmokingswitch"))
defineProperty("nosmokingswitchonoff", globalPropertyi("parshukovedition/nosmokingswitchonoff"))
defineProperty("flightdeckdoor_toggle", globalPropertyf("parshukovedition/flightdeckdoor_toggle"))
defineProperty("flightdeckdoor", globalPropertyf("parshukovedition/flightdeckdoor"))
defineProperty("flightdeckdoor_state", globalPropertyf("parshukovedition/flightdeckdoor_state"))
defineProperty("elev_fail_led", loadImage("lamps.png", 150, 30, 50, 30))
defineProperty("ail_fail_led", loadImage("lamps.png", 200, 30, 50, 30))
defineProperty("pos_not_work_img", loadImage("lamps.png", 50, 60, 50, 30))
defineProperty("ice_on_plane_img", loadImage("lamps.png", 100, 60, 50, 30))
defineProperty("elev_force_led", loadImage("lamps.png", 0, 60, 50, 30))
defineProperty("left_ahz_fail_txt", loadImage("lamps.png", 0, 0, 50, 30))
defineProperty("right_ahz_fail_txt", loadImage("lamps.png", 100, 0, 50, 30))
defineProperty("third_ahz_fail_txt", loadImage("lamps.png", 50, 0, 50, 30))
defineProperty("roll_left_txt", loadImage("lamps.png", 150, 0, 50, 30))
defineProperty("roll_right_txt", loadImage("lamps.png", 200, 0, 50, 30))
defineProperty("check_ahz_txt", loadImage("lamps.png", 0, 30, 50, 30))
defineProperty("critical_mode", loadImage("lamps.png", 50, 30, 50, 30))
defineProperty("warning_img", loadImage("lamps.png", 100, 30, 50, 30))
defineProperty("luk", globalPropertyf("parshukovedition/lukbesson"))
defineProperty("lukbesson_switch", globalPropertyf("parshukovedition/lukbesson_switch"))
defineProperty("overhead_light_left_red", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[10]"))
defineProperty("overhead_light_left_white", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[11]"))
defineProperty("overhead_light_right_red", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[12]"))
defineProperty("overhead_light_right_white", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[13]"))
defineProperty("overhead_light_pilot_right_red", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[14]"))
defineProperty("overhead_light_pilot_right_white", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[15]"))
defineProperty("overhead_light_pilot_left_red", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[8]"))
defineProperty("overhead_light_pilot_left_white", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[9]"))
defineProperty("main_light_white", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[6]"))
defineProperty("main_light_red", globalPropertyf("sim/cockpit2/switches/instrument_brightness_ratio[7]"))
defineProperty("overhead_light_left_ownDR", globalPropertyf("parshukovedition/overhead_lamp_left_bright"))
defineProperty("overhead_light_left_mode", globalPropertyf("parshukovedition/overhead_lamp_left_mode"))
defineProperty("overhead_light_right_ownDR", globalPropertyf("parshukovedition/overhead_lamp_right_bright"))
defineProperty("overhead_light_right_mode", globalPropertyf("parshukovedition/overhead_lamp_right_mode"))
defineProperty("overhead_light_pilot_right_ownDR", globalPropertyf("parshukovedition/overhead_lamp_pilot_right_bright"))
defineProperty("overhead_light_pilot_right_mode", globalPropertyf("parshukovedition/overhead_lamp_pilot_right_mode"))
defineProperty("overhead_light_pilot_left_ownDR", globalPropertyf("parshukovedition/overhead_lamp_pilot_left_bright"))
defineProperty("overhead_light_pilot_left_mode", globalPropertyf("parshukovedition/overhead_lamp_pilot_left_mode"))
defineProperty("main_cabin_light", globalPropertyi("parshukovedition/switch/main_cabin_light"))
defineProperty("main_cabin_light_modeL", globalPropertyi("parshukovedition/switch/main_cabin_light_modeL"))
defineProperty("main_cabin_light_modeR", globalPropertyi("parshukovedition/switch/main_cabin_light_modeR"))
defineProperty("main_cabin_light_mode", globalPropertyf("parshukovedition/switch/main_cabin_light_mode"))
defineProperty("cockpit_panel_light_switch", globalPropertyf("sim/custom/xap/An24_misc/cockpit_panel"))
defineProperty("light1LFD", globalPropertyf("parshukovedition/lights/light1LFD"))
defineProperty("light2LFD", globalPropertyf("parshukovedition/lights/light2LFD"))
defineProperty("light3LFD", globalPropertyf("parshukovedition/lights/light3LFD"))
defineProperty("light4LFD", globalPropertyf("parshukovedition/lights/light4LFD"))
defineProperty("light5LFD", globalPropertyf("parshukovedition/lights/light5LFD"))
defineProperty("light6LFD", globalPropertyf("parshukovedition/lights/light6LFD"))
defineProperty("light7LFD", globalPropertyf("parshukovedition/lights/light7LFD"))
defineProperty("light8LFD", globalPropertyf("parshukovedition/lights/light8LFD"))
defineProperty("light9LFD", globalPropertyf("parshukovedition/lights/light9LFD"))
defineProperty("light1CFD", globalPropertyf("parshukovedition/lights/light1CFD"))
defineProperty("light2CFD", globalPropertyf("parshukovedition/lights/light2CFD"))
defineProperty("light3CFD", globalPropertyf("parshukovedition/lights/light3CFD"))
defineProperty("light4CFD", globalPropertyf("parshukovedition/lights/light4CFD"))
defineProperty("light5CFD", globalPropertyf("parshukovedition/lights/light5CFD"))
defineProperty("light6CFD", globalPropertyf("parshukovedition/lights/light6CFD"))
defineProperty("light7CFD", globalPropertyf("parshukovedition/lights/light7CFD"))
defineProperty("light8CFD", globalPropertyf("parshukovedition/lights/light8CFD"))
defineProperty("light9CFD", globalPropertyf("parshukovedition/lights/light9CFD"))
defineProperty("light1CCFD", globalPropertyf("parshukovedition/lights/light1CCFD"))
defineProperty("light2CCFD", globalPropertyf("parshukovedition/lights/light2CCFD"))
defineProperty("light3CCFD", globalPropertyf("parshukovedition/lights/light3CCFD"))
defineProperty("light4CCFD", globalPropertyf("parshukovedition/lights/light4CCFD"))
defineProperty("light5CCFD", globalPropertyf("parshukovedition/lights/light5CCFD"))
defineProperty("light6CCFD", globalPropertyf("parshukovedition/lights/light6CCFD"))
defineProperty("light7CCFD", globalPropertyf("parshukovedition/lights/light7CCFD"))
defineProperty("light8CCFD", globalPropertyf("parshukovedition/lights/light8CCFD"))
defineProperty("light9CCFD", globalPropertyf("parshukovedition/lights/light9CCFD"))
defineProperty("light1RFD", globalPropertyf("parshukovedition/lights/light1RFD"))
defineProperty("light2RFD", globalPropertyf("parshukovedition/lights/light2RFD"))
defineProperty("light3RFD", globalPropertyf("parshukovedition/lights/light3RFD"))
defineProperty("light4RFD", globalPropertyf("parshukovedition/lights/light4RFD"))
defineProperty("light5RFD", globalPropertyf("parshukovedition/lights/light5RFD"))
defineProperty("light6RFD", globalPropertyf("parshukovedition/lights/light6RFD"))
defineProperty("light7RFD", globalPropertyf("parshukovedition/lights/light7RFD"))
defineProperty("light8RFD", globalPropertyf("parshukovedition/lights/light8RFD"))
defineProperty("light9RFD", globalPropertyf("parshukovedition/lights/light9RFD"))
---------------------------------------------------------------------
defineProperty("light1LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light1LFDGaugesDownLeft"))
defineProperty("light2LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light2LFDGaugesDownLeft"))
defineProperty("light3LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light3LFDGaugesDownLeft"))
defineProperty("light4LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light4LFDGaugesDownLeft"))
defineProperty("light5LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light5LFDGaugesDownLeft"))
defineProperty("light6LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light6LFDGaugesDownLeft"))
defineProperty("light7LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light7LFDGaugesDownLeft"))
defineProperty("light8LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light8LFDGaugesDownLeft"))
defineProperty("light9LFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light9LFDGaugesDownLeft"))
defineProperty("light1LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light1LFDGaugesDownRight"))
defineProperty("light2LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light2LFDGaugesDownRight"))
defineProperty("light3LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light3LFDGaugesDownRight"))
defineProperty("light4LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light4LFDGaugesDownRight"))
defineProperty("light5LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light5LFDGaugesDownRight"))
defineProperty("light6LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light6LFDGaugesDownRight"))
defineProperty("light7LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light7LFDGaugesDownRight"))
defineProperty("light8LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light8LFDGaugesDownRight"))
defineProperty("light9LFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light9LFDGaugesDownRight"))
defineProperty("light1LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light1LFDGaugesUpLeft"))
defineProperty("light2LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light2LFDGaugesUpLeft"))
defineProperty("light3LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light3LFDGaugesUpLeft"))
defineProperty("light4LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light4LFDGaugesUpLeft"))
defineProperty("light5LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light5LFDGaugesUpLeft"))
defineProperty("light6LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light6LFDGaugesUpLeft"))
defineProperty("light7LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light7LFDGaugesUpLeft"))
defineProperty("light8LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light8LFDGaugesUpLeft"))
defineProperty("light9LFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light9LFDGaugesUpLeft"))
defineProperty("light1LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light1LFDGaugesUpRight"))
defineProperty("light2LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light2LFDGaugesUpRight"))
defineProperty("light3LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light3LFDGaugesUpRight"))
defineProperty("light4LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light4LFDGaugesUpRight"))
defineProperty("light5LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light5LFDGaugesUpRight"))
defineProperty("light6LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light6LFDGaugesUpRight"))
defineProperty("light7LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light7LFDGaugesUpRight"))
defineProperty("light8LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light8LFDGaugesUpRight"))
defineProperty("light9LFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light9LFDGaugesUpRight"))
---------------------------------------------------------------------
defineProperty("light1RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light1RFDGaugesDownLeft"))
defineProperty("light2RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light2RFDGaugesDownLeft"))
defineProperty("light3RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light3RFDGaugesDownLeft"))
defineProperty("light4RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light4RFDGaugesDownLeft"))
defineProperty("light5RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light5RFDGaugesDownLeft"))
defineProperty("light6RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light6RFDGaugesDownLeft"))
defineProperty("light7RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light7RFDGaugesDownLeft"))
defineProperty("light8RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light8RFDGaugesDownLeft"))
defineProperty("light9RFDGaugesDownLeft", globalPropertyf("parshukovedition/lights/light9RFDGaugesDownLeft"))
defineProperty("light1RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light1RFDGaugesDownRight"))
defineProperty("light2RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light2RFDGaugesDownRight"))
defineProperty("light3RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light3RFDGaugesDownRight"))
defineProperty("light4RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light4RFDGaugesDownRight"))
defineProperty("light5RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light5RFDGaugesDownRight"))
defineProperty("light6RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light6RFDGaugesDownRight"))
defineProperty("light7RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light7RFDGaugesDownRight"))
defineProperty("light8RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light8RFDGaugesDownRight"))
defineProperty("light9RFDGaugesDownRight", globalPropertyf("parshukovedition/lights/light9RFDGaugesDownRight"))
defineProperty("light1RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light1RFDGaugesUpLeft"))
defineProperty("light2RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light2RFDGaugesUpLeft"))
defineProperty("light3RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light3RFDGaugesUpLeft"))
defineProperty("light4RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light4RFDGaugesUpLeft"))
defineProperty("light5RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light5RFDGaugesUpLeft"))
defineProperty("light6RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light6RFDGaugesUpLeft"))
defineProperty("light7RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light7RFDGaugesUpLeft"))
defineProperty("light8RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light8RFDGaugesUpLeft"))
defineProperty("light9RFDGaugesUpLeft", globalPropertyf("parshukovedition/lights/light9RFDGaugesUpLeft"))
defineProperty("light1RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light1RFDGaugesUpRight"))
defineProperty("light2RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light2RFDGaugesUpRight"))
defineProperty("light3RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light3RFDGaugesUpRight"))
defineProperty("light4RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light4RFDGaugesUpRight"))
defineProperty("light5RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light5RFDGaugesUpRight"))
defineProperty("light6RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light6RFDGaugesUpRight"))
defineProperty("light7RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light7RFDGaugesUpRight"))
defineProperty("light8RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light8RFDGaugesUpRight"))
defineProperty("light9RFDGaugesUpRight", globalPropertyf("parshukovedition/lights/light9RFDGaugesUpRight"))
-----------------------------------------------------------------------
defineProperty("light1OHL", globalPropertyf("parshukovedition/lights/light1OHL"))
defineProperty("light2OHL", globalPropertyf("parshukovedition/lights/light2OHL"))
defineProperty("light3OHL", globalPropertyf("parshukovedition/lights/light3OHL"))
defineProperty("light4OHL", globalPropertyf("parshukovedition/lights/light4OHL"))
defineProperty("light5OHL", globalPropertyf("parshukovedition/lights/light5OHL"))
defineProperty("light6OHL", globalPropertyf("parshukovedition/lights/light6OHL"))
defineProperty("light7OHL", globalPropertyf("parshukovedition/lights/light7OHL"))
defineProperty("light8OHL", globalPropertyf("parshukovedition/lights/light8OHL"))
defineProperty("light9OHL", globalPropertyf("parshukovedition/lights/light9OHL"))
defineProperty("light1OHR", globalPropertyf("parshukovedition/lights/light1OHR"))
defineProperty("light2OHR", globalPropertyf("parshukovedition/lights/light2OHR"))
defineProperty("light3OHR", globalPropertyf("parshukovedition/lights/light3OHR"))
defineProperty("light4OHR", globalPropertyf("parshukovedition/lights/light4OHR"))
defineProperty("light5OHR", globalPropertyf("parshukovedition/lights/light5OHR"))
defineProperty("light6OHR", globalPropertyf("parshukovedition/lights/light6OHR"))
defineProperty("light7OHR", globalPropertyf("parshukovedition/lights/light7OHR"))
defineProperty("light8OHR", globalPropertyf("parshukovedition/lights/light8OHR"))
defineProperty("light9OHR", globalPropertyf("parshukovedition/lights/light9OHR"))
defineProperty("light1FoR", globalPropertyf("parshukovedition/lights/light1FoR"))
defineProperty("light2FoR", globalPropertyf("parshukovedition/lights/light2FoR"))
defineProperty("light3FoR", globalPropertyf("parshukovedition/lights/light3FoR"))
defineProperty("light4FoR", globalPropertyf("parshukovedition/lights/light4FoR"))
defineProperty("light5FoR", globalPropertyf("parshukovedition/lights/light5FoR"))
defineProperty("light6FoR", globalPropertyf("parshukovedition/lights/light6FoR"))
defineProperty("light7FoR", globalPropertyf("parshukovedition/lights/light7FoR"))
defineProperty("light8FoR", globalPropertyf("parshukovedition/lights/light8FoR"))
defineProperty("light9FoR", globalPropertyf("parshukovedition/lights/light9FoR"))
defineProperty("light1PiL", globalPropertyf("parshukovedition/lights/light1PiL"))
defineProperty("light2PiL", globalPropertyf("parshukovedition/lights/light2PiL"))
defineProperty("light3PiL", globalPropertyf("parshukovedition/lights/light3PiL"))
defineProperty("light4PiL", globalPropertyf("parshukovedition/lights/light4PiL"))
defineProperty("light5PiL", globalPropertyf("parshukovedition/lights/light5PiL"))
defineProperty("light6PiL", globalPropertyf("parshukovedition/lights/light6PiL"))
defineProperty("light7PiL", globalPropertyf("parshukovedition/lights/light7PiL"))
defineProperty("light8PiL", globalPropertyf("parshukovedition/lights/light8PiL"))
defineProperty("light9PiL", globalPropertyf("parshukovedition/lights/light9PiL"))

plusmainlight = CCom("parshukovedition/lights/plusmainlight")
minusmainlight = CCom("parshukovedition/lights/minusmainlight")
function plusmainlight_cmd(phase)
if 0 == phase then
	if get(main_cabin_light_mode)<2 then
		set(main_cabin_light_mode,get(main_cabin_light_mode)+1)
	end
return 1
end
end
regCom(plusmainlight, plusmainlight_cmd)
function minusmainlight_cmd(phase)
if 0 == phase then
	if get(main_cabin_light_mode)>0 then
		set(main_cabin_light_mode,get(main_cabin_light_mode)-1)
	end
return 1
end
end
regCom(minusmainlight, minusmainlight_cmd)
set(light1LFD,1)
set(light2LFD,0)
set(light3LFD,0)
set(light4LFD,0)
set(light5LFD,0.035)
set(light6LFD,0.0001)
set(light7LFD,0)
set(light8LFD,-0.5)
set(light9LFD,-0.7171)
set(light1CFD,1)
set(light2CFD,0)
set(light3CFD,0)
set(light4CFD,0)
set(light5CFD,0.035)
set(light6CFD,0.0001)
set(light7CFD,0)
set(light8CFD,0)
set(light9CFD,-1)
set(light1CCFD,1)
set(light2CCFD,0)
set(light3CCFD,0)
set(light4CCFD,0)
set(light5CCFD,0.035)
set(light6CCFD,0.0001)
set(light7CCFD,0)
set(light8CCFD,-0.5)
set(light9CCFD,-0.7171)
set(light1RFD,1)
set(light2RFD,0)
set(light3RFD,0)
set(light4RFD,0)
set(light5RFD,0.035)
set(light6RFD,0.0001)
set(light7RFD,0)
set(light8RFD,-0.5)
set(light9RFD,-0.7171)

set(light1OHL,1)
set(light2OHL,0)
set(light3OHL,0)
set(light4OHL,0)
set(light5OHL,0.03)
set(light6OHL,0.65)
set(light7OHL,0)
set(light8OHL,0.7)
set(light9OHL,1)
set(light1OHR,1)
set(light2OHR,0)
set(light3OHR,0)
set(light4OHR,0)
set(light5OHR,0.03)
set(light6OHR,0.65)
set(light7OHR,0)
set(light8OHR,0.7)
set(light9OHR,1)
set(light1FoR,1)
set(light2FoR,0)
set(light3FoR,0)
set(light4FoR,0)
set(light5FoR,0.03)
set(light6FoR,0.65)
set(light7FoR,0)
set(light8FoR,1)
set(light9FoR,0)
set(light1PiL,1)
set(light2PiL,0)
set(light3PiL,0)
set(light4PiL,0)
set(light5PiL,0.03)
set(light6PiL,0.65)
set(light7PiL,0)
set(light8PiL,1)
set(light9PiL,0)
---
set(light1LFDGaugesDownLeft,1)
set(light2LFDGaugesDownLeft,0.66)
set(light3LFDGaugesDownLeft,0.18)
set(light5LFDGaugesDownLeft,0.1)
set(light6LFDGaugesDownLeft,0.35)
set(light7LFDGaugesDownLeft,-0.4)
set(light8LFDGaugesDownLeft,-1)
set(light9LFDGaugesDownLeft,-0.2)
set(light1LFDGaugesDownRight,1)
set(light2LFDGaugesDownRight,0.66)
set(light3LFDGaugesDownRight,0.18)
set(light5LFDGaugesDownRight,0.1)
set(light6LFDGaugesDownRight,0.35)
set(light7LFDGaugesDownRight,0.4)
set(light8LFDGaugesDownRight,-1)
set(light9LFDGaugesDownRight,-0.2)
set(light1LFDGaugesUpLeft,1)
set(light2LFDGaugesUpLeft,0.66)
set(light3LFDGaugesUpLeft,0.18)
set(light5LFDGaugesUpLeft,0.1)
set(light6LFDGaugesUpLeft,0.35)
set(light7LFDGaugesUpLeft,-0.4)
set(light8LFDGaugesUpLeft,-0.2)
set(light9LFDGaugesUpLeft,-0.2)
set(light1LFDGaugesUpRight,1)
set(light2LFDGaugesUpRight,0.66)
set(light3LFDGaugesUpRight,0.18)
set(light5LFDGaugesUpRight,0.1)
set(light6LFDGaugesUpRight,0.35)
set(light7LFDGaugesUpRight,0.4)
set(light8LFDGaugesUpRight,0.2)
set(light9LFDGaugesUpRight,-0.2)
----
set(light1RFDGaugesDownLeft,1)
set(light2RFDGaugesDownLeft,0.66)
set(light3RFDGaugesDownLeft,0.18)
set(light5RFDGaugesDownLeft,0.1)
set(light6RFDGaugesDownLeft,0.35)
set(light7RFDGaugesDownLeft,-0.4)
set(light8RFDGaugesDownLeft,-1)
set(light9RFDGaugesDownLeft,-0.2)
set(light1RFDGaugesDownRight,1)
set(light2RFDGaugesDownRight,0.66)
set(light3RFDGaugesDownRight,0.18)
set(light5RFDGaugesDownRight,0.1)
set(light6RFDGaugesDownRight,0.35)
set(light7RFDGaugesDownRight,0.4)
set(light8RFDGaugesDownRight,-1)
set(light9RFDGaugesDownRight,-0.2)
set(light1RFDGaugesUpLeft,1)
set(light2RFDGaugesUpLeft,0.66)
set(light3RFDGaugesUpLeft,0.18)
set(light5RFDGaugesUpLeft,0.1)
set(light6RFDGaugesUpLeft,0.35)
set(light7RFDGaugesUpLeft,-0.4)
set(light8RFDGaugesUpLeft,-0.2)
set(light9RFDGaugesUpLeft,-0.2)
set(light1RFDGaugesUpRight,1)
set(light2RFDGaugesUpRight,0.66)
set(light3RFDGaugesUpRight,0.18)
set(light5RFDGaugesUpRight,0.1)
set(light6RFDGaugesUpRight,0.35)
set(light7RFDGaugesUpRight,0.4)
set(light8RFDGaugesUpRight,0.2)
set(light9RFDGaugesUpRight,-0.2)
-- commands
autopilot_off_on = findCommand("sim/autopilot/fdir_servos_down_one")
local autopilot_off_onn_Sound = loadSample('Sounds/alert/autopilot_disco.wav')
set(luk,0)
local time_isalert = 0
local last_lamp_change = get(flight_time)
--local isalerton =0
--playSample(switch_sound, 0)


function update()
set(light4CCFD,get(light4CFD)*0.4)

set(light4LFDGaugesDownRight,get(light4LFDGaugesDownLeft))
set(light4LFDGaugesUpRight,get(light4LFDGaugesDownLeft))
set(light4LFDGaugesUpLeft,get(light4LFDGaugesDownLeft))
set(light4RFDGaugesDownRight,get(light4RFDGaugesDownLeft))
set(light4RFDGaugesUpRight,get(light4RFDGaugesDownLeft))
set(light4RFDGaugesUpLeft,get(light4RFDGaugesDownLeft))

if get(lukbesson_switch) ==1 then
	if get(luk)<1 then
		set(luk,get(luk)+0.1)
	elseif get(luk)>1 then
		set(luk,1)
	end
else
	if get(luk)> 0 then
		set(luk,get(luk)-0.1)
	elseif get(luk) < 0 then
		set(luk,0)
	end
end

local power27 = get(bus_DC_27_volt)

if get(test_lamp_pilot1_switch)==1 or get(test_lamp_pilot2_switch)==1 then
if get(power27) > 21 then
	set(test_lamp_pilot,1)
end
else
set(test_lamp_pilot,0)
end



if get(overhead_light_left_mode)==1 then
if get(power27) > 21 then
set(light2OHL,0)
set(light3OHL,0)
set(light4OHL,get(overhead_light_left_ownDR)*10)
set(overhead_light_left_red,get(overhead_light_left_ownDR))
set(overhead_light_left_white,0)
else
set(overhead_light_left_red,0)
set(overhead_light_left_white,0)
set(light4OHL,0)
end
elseif get(overhead_light_left_mode)==2 then
if get(power27) > 21 then
set(light2OHL,1)
set(light3OHL,1)
set(light4OHL,get(overhead_light_left_ownDR)*10)
set(overhead_light_left_red,0)
set(overhead_light_left_white,get(overhead_light_left_ownDR))
else
set(overhead_light_left_red,0)
set(overhead_light_left_white,0)
set(light4OHL,0)
end
else
set(light4OHL,0)
set(overhead_light_left_white,0)
set(overhead_light_left_red,0)
end

---------overhead right lamps
if get(overhead_light_right_mode)==1 then
if get(power27) > 21 then
set(light2OHR,0)
set(light3OHR,0)
set(light4OHR,get(overhead_light_right_ownDR)*10)
set(overhead_light_right_red,get(overhead_light_right_ownDR))
set(overhead_light_right_white,0)
else
set(overhead_light_right_red,0)
set(overhead_light_right_white,0)
set(light4OHR,0)
end
elseif get(overhead_light_right_mode)==2 then
if get(power27) > 21 then
set(light2OHR,1)
set(light3OHR,1)
set(light4OHR,get(overhead_light_right_ownDR)*10)
set(overhead_light_right_red,0)
set(overhead_light_right_white,get(overhead_light_right_ownDR))
else
set(overhead_light_right_red,0)
set(overhead_light_right_white,0)
set(light4OHR,0)
end
else
set(light4OHR,0)
set(overhead_light_right_white,0)
set(overhead_light_right_red,0)
end
-----------second pilot lamp
if get(overhead_light_pilot_right_mode)==1 then
if get(power27) > 21 then
set(light2FoR,0)
set(light3FoR,0)
set(light4FoR,get(overhead_light_pilot_right_ownDR)*10)
set(overhead_light_pilot_right_red,get(overhead_light_pilot_right_ownDR))
set(overhead_light_pilot_right_white,0)
else
set(overhead_light_pilot_right_red,0)
set(overhead_light_pilot_right_white,0)
set(light4FoR,0)
end
elseif get(overhead_light_pilot_right_mode)==2 then
if get(power27) > 21 then
set(light2FoR,1)
set(light3FoR,1)
set(light4FoR,get(overhead_light_pilot_right_ownDR)*10)
set(overhead_light_pilot_right_red,0)
set(overhead_light_pilot_right_white,get(overhead_light_pilot_right_ownDR))
else
set(overhead_light_pilot_right_red,0)
set(overhead_light_pilot_right_white,0)
set(light4FoR,0)
end
else
set(light4FoR,0)
set(overhead_light_pilot_right_white,0)
set(overhead_light_pilot_right_red,0)
end
-----------left pilot lamp
if get(overhead_light_pilot_left_mode)==1 then
if get(power27) > 21 then
set(light2PiL,0)
set(light3PiL,0)
set(light4PiL,get(overhead_light_pilot_left_ownDR)*10)
set(overhead_light_pilot_left_red,get(overhead_light_pilot_left_ownDR))
set(overhead_light_pilot_left_white,0)
else
set(overhead_light_pilot_left_red,0)
set(overhead_light_pilot_left_white,0)
set(light4PiL,0)
end
elseif get(overhead_light_pilot_left_mode)==2 then
if get(power27) > 21 then
set(light2PiL,1)
set(light3PiL,1)
set(light4PiL,get(overhead_light_pilot_left_ownDR)*10)
set(overhead_light_pilot_left_red,0)
set(overhead_light_pilot_left_white,get(overhead_light_pilot_left_ownDR))
else
set(overhead_light_pilot_left_red,0)
set(overhead_light_pilot_left_white,0)
end
else
set(light4PiL,0)
set(overhead_light_pilot_left_white,0)
set(overhead_light_pilot_left_red,0)
end
------------------------
---�������� ��������� � ������

---���, ��� �������� ������� �� �����
if get(power27) > 21 then
	
	if get(main_cabin_light)==1 then
		if get(main_cabin_light_mode)==2 then
			set(main_light_white,1)
		else
			set(main_light_white,0)
		end
		if get(main_cabin_light_mode)==0 then
			set(main_light_red,1)
		else
			set(main_light_red,0)
		end
	else
		set(main_light_white,0)
		set(main_light_red,0)
	end
	if get(nosmokingswitchonoff)==1 then
	set(nosmokingswitch,1)
	else
	set(nosmokingswitch,0)
	end
	if get(cockpit_panel_light_switch)==1 then
	set(light1LFD,1)
	set(light1CFD,1)
	set(light1CCFD,1)
	set(light1RFD,1)
	set(light5LFDGaugesDownLeft,0.1)
	set(light5LFDGaugesDownRight,0.1)
	set(light5LFDGaugesUpLeft,0.1)
	set(light5LFDGaugesUpRight,0.1)
	set(light5RFDGaugesDownLeft,0.1)
	set(light5RFDGaugesDownRight,0.1)
	set(light5RFDGaugesUpLeft,0.1)
	set(light5RFDGaugesUpRight,0.1)
	elseif get(cockpit_panel_light_switch)==(-1) then
	set(light1LFD,1)
	set(light1CFD,1)
	set(light1CCFD,1)
	set(light1RFD,0)
	set(light5LFDGaugesDownLeft,0.1)
	set(light5LFDGaugesDownRight,0.1)
	set(light5LFDGaugesUpLeft,0.1)
	set(light5LFDGaugesUpRight,0.1)
	set(light5RFDGaugesDownLeft,0)
	set(light5RFDGaugesDownRight,0)
	set(light5RFDGaugesUpLeft,0)
	set(light5RFDGaugesUpRight,0)
	else
	set(light1LFD,0)
	set(light1CFD,0)
	set(light1CCFD,0)
	set(light1RFD,0)
	set(light5LFDGaugesDownLeft,0)
	set(light5LFDGaugesDownRight,0)
	set(light5LFDGaugesUpLeft,0)
	set(light5LFDGaugesUpRight,0)
	set(light5RFDGaugesDownLeft,0)
	set(light5RFDGaugesDownRight,0)
	set(light5RFDGaugesUpLeft,0)
	set(light5RFDGaugesUpRight,0)
	end
else
	set(main_light_white,0)
	set(main_light_red,0)
	set(light1LFD,0)
	set(light1CFD,0)
	set(light1CCFD,0)
	set(light1RFD,0)
	set(light5LFDGaugesDownLeft,0)
	set(light5LFDGaugesDownRight,0)
	set(light5LFDGaugesUpLeft,0)
	set(light5LFDGaugesUpRight,0)
	set(light5RFDGaugesDownLeft,0)
	set(light5RFDGaugesDownRight,0)
	set(light5RFDGaugesUpLeft,0)
	set(light5RFDGaugesUpRight,0)
end
	if get(flightdeckdoor_toggle)==1 then
		if get(flightdeckdoor_state)<1 then
			if get(flightdeckdoor)<1  then
				set(flightdeckdoor,get(flightdeckdoor)+0.04)
				set(flightdeckdoor_state,get(flightdeckdoor_state)+0.04)
			end

		end
	end
	if get(flightdeckdoor_toggle)==0 then
		if get(flightdeckdoor_state)>0 then
			if get(flightdeckdoor)>0 then
				set(flightdeckdoor,get(flightdeckdoor)-0.04)
				set(flightdeckdoor_state,get(flightdeckdoor_state)-0.04)
			end

		end
	end
	if get(flightdeckdoor)<0 then
	set(flightdeckdoor,0)
	end
	if get(flightdeckdoor_state)<0 then
	set(flightdeckdoor_state,0)
	end




	----logic PF button anim
	if get(autopilot_state_PF) > 1 then
	 set(autopilot_state_PF,1)
	end
	if get(autopilot_state_FO) > 1 then
	 set(autopilot_state_FO,1)
	end
	if get(PF_ApbuttonState) < 1 then
			set(autopilot_state_PF,get(autopilot_state_PF)+0.1)
			set(PF_ApbuttonState,get(PF_ApbuttonState)+0.1)
	end
	if get(PF_ApbuttonState) == 1 then
			set(autopilot_state_PF,get(autopilot_state_PF)+0.1)
			set(PF_ApbuttonState,get(PF_ApbuttonState)+0.1)
	end
	if get(PF_ApbuttonState)<2 then
		if get(PF_ApbuttonState)>1 then
			set(autopilot_state_PF,get(autopilot_state_PF)-0.1)
			set(PF_ApbuttonState,get(PF_ApbuttonState)+0.1)
		end
	end
	if get(PF_ApbuttonState)==2 or (get(PF_ApbuttonState)>2 and get(PF_ApbuttonState)<3)then
				set(autopilot_state_PF_button,0)
				set(autopilot_state_PF,0)
				set(PF_ApbuttonState,3)
				--set(isalerton,0)
	end
	---logic FO button anim
	if get(FO_ApbuttonState) < 1 then
			set(autopilot_state_FO,get(autopilot_state_FO)+0.1)
			set(FO_ApbuttonState,get(FO_ApbuttonState)+0.1)
	end
	if get(FO_ApbuttonState) == 1 then
			set(autopilot_state_FO,get(autopilot_state_FO)+0.1)
			set(FO_ApbuttonState,get(FO_ApbuttonState)+0.1)
	end
	if get(FO_ApbuttonState)<2 then
		if get(FO_ApbuttonState)>1 then
			set(autopilot_state_FO,get(autopilot_state_FO)-0.1)
			set(FO_ApbuttonState,get(FO_ApbuttonState)+0.1)
		end
	end

	if get(FO_ApbuttonState)==2 or (get(FO_ApbuttonState)>2 and get(FO_ApbuttonState)<3)then
				set(autopilot_state_FO_button,0)
				set(autopilot_state_FO,0)
				set(FO_ApbuttonState,3)
				--set(isalerton,0)
	end
	if get(isalerton)==1 then
	set(isalerton,2)
	time_isalert=get(flight_time)
	end
	if get(isalerton)==2 then
	if (get(flight_time)-time_isalert)<3 then
	else
	set(isalerton,0)
	end
	end

	if get(autopilot_state_PF_button)==1 then
		set(PF_ApbuttonState,0)
		if get(ap_on_lit)==1 then
			playSample(autopilot_off_onn_Sound, 0)
			set(isalerton,1)
			commandOnce(autopilot_off_on)
		else
			set(autopilot_state_PF_button,0)
		end
	end
	if get(autopilot_state_FO_button)==1 then
		set(FO_ApbuttonState,0)
		if get(ap_on_lit)==1 then
			playSample(autopilot_off_onn_Sound, 0)
			set(isalerton,1)
			commandOnce(autopilot_off_on)
		else
			set(autopilot_state_FO_button,0)
		end
	end
--local power27 = get(bus_DC_27_volt)
if get(power27) > 21 then
if get(testmsrp)==1 then
if get(msrp_switch)==1 then
	if get(flight_time) - last_lamp_change > math.random(0.28,0.46) then
				set(msrplight,1)
				last_lamp_change = get(flight_time)
				else
				set(msrplight,0)
			end
end
end
end

if get(testmsrp_cap)==0 then
if get(testmsrp)==1 then
set(testmsrp,0)
end
end

if get(msrp_switch_cap)==0 then
if get(msrp_switch)==1 then
set(msrp_switch,0)
end

end
--------



end


components = {

textureLit {
		position = {1284, 517, 50, 30},
		image = get(elev_fail_led),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1340, 517, 50, 30},
		image = get(ail_fail_led),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1116, 450, 50, 30},
		image = get(elev_fail_led),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1061, 450, 50, 30},
		image = get(ail_fail_led),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1229, 517, 50, 30},
		image = get(elev_force_led),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1172, 450, 50, 30},
		image = get(elev_force_led),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1398, 518, 45, 27},
		image = get(left_ahz_fail_txt),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1455, 518, 43, 27},
		image = get(third_ahz_fail_txt),
		visible = function()
			return get(test_lamp_pilot) == 1
		end
	},
textureLit {
		position = {1007, 484, 45, 27},
		image = get(right_ahz_fail_txt),
		visible = function()
			return get(test_lamp_pilot) == 1
		end,
	},


	-- left roll indicator
	textureLit {
		position = {1062, 484, 45, 27},
		image = get(roll_left_txt),
		visible = function()
			return get(test_lamp_pilot) == 1
		end,
	},
	-- right roll indicator
	textureLit {
		position = {1119, 484, 43, 27},
		image = get(roll_right_txt),
		visible = function()
			return get(test_lamp_pilot) == 1
		end,
	},

	-- check ahz
	textureLit {
		position = {1174, 484, 43, 27},
		image = get(check_ahz_txt),
		visible = function()
			return get(test_lamp_pilot) == 1
		end,
	},


























textureLit {
		position = {740, 309, 20, 20},
		image = get(green_led),
		visible = function()
			return get(msrplight) == 1
		end
	},

textureLit {
		image = get(elev_fail_led),
		position = {1284, 517, 50, 30},
		visible = function()
			return get(isalerton)==2--(get(autopilot_state_PF) > 0 or get(autopilot_state_FO) > 0) and get(isalerton)==2

		end,
	},
	textureLit {
		image = get(elev_fail_led),
		position = {1116, 450, 50, 30},
		visible = function()
			return get(isalerton)==2--(get(autopilot_state_PF) > 0 or get(autopilot_state_FO) > 0) and get(isalerton)==2
		end,
	},
	textureLit {
		image = get(ail_fail_led),
		position = {1340, 517, 50, 30},
		visible = function()
			return get(isalerton)==2--(get(autopilot_state_PF) >0 or get(autopilot_state_FO) > 0) and get(isalerton)==2
		end,
	},
	textureLit {
		image = get(ail_fail_led),
		position = {1061, 450, 50, 30},
		visible = function()
			return get(isalerton)==2--(get(autopilot_state_PF) >0 or get(autopilot_state_FO) > 0) and get(isalerton)==2
		end,
	},


}