AN-24RV Aeroflot Nord paint for X-Plane v9 model by Felis, available from the store at www.x-plane.org. 

To install, simply place the unzipped folder 'Aeroflot Nord' into the liveries folder of the AN-24.

If you want to use this as the default paint: Open the aircraft in PlaneMaker, select the livery (as in XP), then save & close PlaneMaker. Then move the file 'An24_icon.png' from the livery folder to the aircraft's folder, (move the original to keep it). This will display the new livery as an icon in XP.

Created with the paint files provided with the aircraft. Please note the aircraft is Payware, and respect the copyright of the original author.
Paint as modified � 2010 Will Davies (amerrir)



